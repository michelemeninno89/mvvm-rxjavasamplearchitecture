package android.mobile.micmen.testforglobal;


import android.mobile.micmen.testforglobal.factory.RepositoryFactory;
import android.mobile.micmen.testforglobal.factory.ServiceFactory;
import android.mobile.micmen.testforglobal.factory.impl.RepositoryFactoryImpl;
import android.mobile.micmen.testforglobal.factory.impl.ServiceFactoryImpl;
import android.mobile.micmen.testforglobal.network.fetching_data.FetchingDataService;
import android.mobile.micmen.testforglobal.repository.fetchingdata.FetchingDataRepository;
import android.mobile.micmen.testforglobal.repository.fetchingdata.impl.FetchingDataMapper;

import junit.framework.Assert;

import org.junit.Test;

import io.reactivex.schedulers.Schedulers;

public class FetchingDataRepositoryTest {

    @Test
    public void testFetchingData() {
        RepositoryFactory repositoryFactory = new RepositoryFactoryImpl();
        FetchingDataRepository fetchingDataRepository = repositoryFactory.makeFetchingDataRepository();
        fetchingDataRepository.fetchNewData().blockingSubscribe(
                dataFetched ->
                {
                    Assert.assertNotNull(dataFetched.getValue());
                }
        );
    }

    @Test
    public void testFetchingDataService() {
        ServiceFactory serviceFactory = new ServiceFactoryImpl();
        Assert.assertNotNull(serviceFactory.makeService(FetchingDataService.class));
    }

    @Test
    public void testFetchingDataMapper() {
        FetchingDataMapper mapper = new FetchingDataMapper();
        ServiceFactory serviceFactory = new ServiceFactoryImpl();
        FetchingDataService fetchingDataService = serviceFactory.makeService(FetchingDataService.class);
        fetchingDataService
                .getFetchingData()
                .observeOn(Schedulers.io())
                .blockingSubscribe(fetchingDataResponse -> Assert.assertNotNull(mapper.map(fetchingDataResponse)) );
    }


}
