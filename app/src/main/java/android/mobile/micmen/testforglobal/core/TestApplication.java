package android.mobile.micmen.testforglobal.core;

import android.app.Application;
import android.mobile.micmen.testforglobal.factory.RepositoryFactory;
import android.mobile.micmen.testforglobal.factory.impl.RepositoryFactoryImpl;

/**
 * <h1>Global Application singleton<h1/>
 *
 * provides repositories
 *
 * @author Michele Meninno
 */

public class TestApplication extends Application {

    private RepositoryFactory repositoryFactory = new RepositoryFactoryImpl();
    private static TestApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static TestApplication getInstance(){
        return instance;
    }

    public RepositoryFactory getRepositoryFactory() {
        return repositoryFactory;
    }
}
