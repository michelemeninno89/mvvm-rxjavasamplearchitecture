package android.mobile.micmen.testforglobal.repository.fetchingdata;

import android.mobile.micmen.testforglobal.repository.fetchingdata.model.DataFetched;

import io.reactivex.Observable;

public interface FetchingDataRepository {

    Observable<DataFetched> fetchNewData();
}
