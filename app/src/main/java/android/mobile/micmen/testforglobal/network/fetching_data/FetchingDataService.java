package android.mobile.micmen.testforglobal.network.fetching_data;

import android.mobile.micmen.testforglobal.network.fetching_data.dto.FetchingDataResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface FetchingDataService {

    @GET("6c6f7f0a-5042-11e8-bc3d-f45c89c9c1ad/")
    Observable<FetchingDataResponse> getFetchingData();

}
