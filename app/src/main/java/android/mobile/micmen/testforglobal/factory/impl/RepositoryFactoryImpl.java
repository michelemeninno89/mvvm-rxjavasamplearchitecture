package android.mobile.micmen.testforglobal.factory.impl;

import android.mobile.micmen.testforglobal.factory.RepositoryFactory;
import android.mobile.micmen.testforglobal.factory.ServiceFactory;
import android.mobile.micmen.testforglobal.network.fetching_data.FetchingDataService;
import android.mobile.micmen.testforglobal.repository.fetchingdata.FetchingDataRepository;
import android.mobile.micmen.testforglobal.repository.fetchingdata.impl.FetchingDataRepositoryImpl;

/**
 * <h1> Repository Factory Implementation<h1/>
 *
 * creates actual repositories
 *
 * @author Michele Meninno
 */

public class RepositoryFactoryImpl implements RepositoryFactory {

    private ServiceFactory serviceFactory = new ServiceFactoryImpl();

    @Override
    public FetchingDataRepository makeFetchingDataRepository() {
        return new FetchingDataRepositoryImpl(serviceFactory.makeService(FetchingDataService.class));
    }
}
