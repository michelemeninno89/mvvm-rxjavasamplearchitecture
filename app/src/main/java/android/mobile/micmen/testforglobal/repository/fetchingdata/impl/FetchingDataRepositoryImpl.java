package android.mobile.micmen.testforglobal.repository.fetchingdata.impl;

import android.mobile.micmen.testforglobal.network.fetching_data.FetchingDataService;
import android.mobile.micmen.testforglobal.repository.fetchingdata.FetchingDataRepository;
import android.mobile.micmen.testforglobal.repository.fetchingdata.model.DataFetched;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * <h1>Fetching data repository implementation</h1>
 *
 * set the background test for network calls
 * maps the dto obtained from the network with the model object
 *
 */
public class FetchingDataRepositoryImpl implements FetchingDataRepository {

    private FetchingDataService service;
    private FetchingDataMapper mapper = new FetchingDataMapper();

    public FetchingDataRepositoryImpl(FetchingDataService service) {
        this.service = service;
    }

    @Override
    public Observable<DataFetched> fetchNewData() {

        return service.getFetchingData()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(fetchingDataResponse -> mapper.map(fetchingDataResponse));
    }
}
