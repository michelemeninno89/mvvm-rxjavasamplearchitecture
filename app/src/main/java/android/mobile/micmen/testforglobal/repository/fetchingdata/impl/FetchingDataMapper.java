package android.mobile.micmen.testforglobal.repository.fetchingdata.impl;

import android.mobile.micmen.testforglobal.core.Mapper;
import android.mobile.micmen.testforglobal.network.fetching_data.dto.FetchingDataResponse;
import android.mobile.micmen.testforglobal.repository.fetchingdata.model.DataFetched;

public class FetchingDataMapper implements Mapper<FetchingDataResponse, DataFetched>{

    @Override
    public DataFetched map(FetchingDataResponse elementToMap) {
        return new DataFetched(elementToMap.getResponseCode());
    }
}
