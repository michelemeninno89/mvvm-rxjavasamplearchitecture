package android.mobile.micmen.testforglobal.factory;

import android.mobile.micmen.testforglobal.repository.fetchingdata.FetchingDataRepository;

/**
 * Provides a list of repository to build in the project
 *
 * @author Michele Meninno
 */
public interface RepositoryFactory {

    FetchingDataRepository makeFetchingDataRepository();

}
